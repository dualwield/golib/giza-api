package main

import (
	"fmt"
	"gitlab.com/dualwield/golib/giza-api"
)

func main() {
	client := giza_api.New("ws://localhost:8077/giza/rqrs-zam")
	if client == nil {
		fmt.Println("got nil service")
		return
	}
	rs, err := client.Send(giza_api.GizaRQ{
		Operator: "=",
		Scope:    "dev",
		Key:      "nok",
		//Payload:  []byte("hello dang world"),
	})
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(string(rs.Payload))
}
