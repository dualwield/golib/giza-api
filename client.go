package giza_api

import (
	"encoding/base64"
	"fmt"
	"github.com/gorilla/websocket"
	"net/http"
	"sync"
)

func New(url string) *Client {
	c := Client{
		watch: nil,
	}

	baHeader := http.Header{
		"Authorization": []string{fmt.Sprintf("Basic %s", base64.URLEncoding.EncodeToString([]byte(fmt.Sprintf("%s:%s", "x", "y"))))},
	}
	dialer := websocket.DefaultDialer
	conn, rs, err := dialer.Dial(url, baHeader)
	if err != nil {
		fmt.Printf("failed to dial %s\n", url)
	}
	c.rqrs = conn
	c.token = rs.Header.Get("GIZA_CLIENT_TOKEN")
	if c.token == "" {
		c.token = "jab"
	}
	//TODO check to make sure token is there
	return &c
}

type Client struct {
	token string
	rqrs  *websocket.Conn
	watch *websocket.Conn
	mutex sync.Mutex
}

func (c *Client) Close() error {
	c.rqrs.Close()
	c.watch.Close()
	return nil
}

func (c *Client) Send(rq GizaRQ) (rs GizaRS, err error) {
	c.mutex.Lock()
	defer c.mutex.Unlock()
	wg := sync.WaitGroup{}
	wg.Add(1)

	go func() {
		for {
			_, p, _err := c.rqrs.ReadMessage()
			if _err != nil {
				rs = GizaRS{
					Code:    9,
					Key:     rq.Key,
					Payload: nil,
				}
				err = _err
				wg.Done()
				return
			}
			rs, err = ParseGizaRS(rq.Key, p)
			wg.Done()
		}
	}()

	if err = c.rqrs.WriteMessage(websocket.TextMessage, rq.Crunch(c.token)); err != nil {
		wg.Done()
		rs = GizaRS{
			Code:    9,
			Key:     rq.Key,
			Payload: nil,
		}
	}

	wg.Wait()
	return

}

type GizaRQ struct {
	Operator string
	Scope    string
	Key      string
	Payload  []byte
}

const (
	OperatorPut    = "+"
	OperatorGet    = "="
	OperatorDel    = "-"
	RequestFormat  = "%s %s %s %s %s" // {operator} {meta} {scope} {key} {base64-data}
	ResponseFormat = "%d %s %s"       // {code} {meta} {base64-data}
)

func (g GizaRQ) Crunch(processorID string) []byte {
	fmt.Println(g)
	switch g.Operator {
	case OperatorPut:
		fmt.Println("in put")
		fmt.Println(processorID)
		x := fmt.Sprintf(RequestFormat, OperatorPut, processorID, g.Scope, base64.URLEncoding.EncodeToString([]byte(g.Key)), base64.URLEncoding.EncodeToString(g.Payload))
		fmt.Println(x)
		return []byte(x)
	case OperatorGet:
		return []byte(fmt.Sprintf(RequestFormat, OperatorGet, processorID, g.Scope, base64.URLEncoding.EncodeToString([]byte(g.Key)), "-"))
	case OperatorDel:
		return []byte(fmt.Sprintf(RequestFormat, OperatorDel, processorID, g.Scope, base64.URLEncoding.EncodeToString([]byte(g.Key)), "-"))
	}
	return nil
}

func ParseGizaRS(key string, payload []byte) (GizaRS, error) {
	rs := GizaRS{Key: key}
	var b64P string
	_, err := fmt.Sscanf(string(payload), ResponseFormat, &rs.Code, &rs.meta, &b64P)
	if err != nil {
		fmt.Println(err)
		return rs, err
	}
	if b64P != "-" {
		rs.Payload, err = base64.URLEncoding.DecodeString(b64P)
		if err != nil {
			fmt.Println(err)
			return rs, err
		}
	}
	return rs, nil
}

type GizaRS struct {
	Code    int
	Key     string
	meta    string
	Payload []byte
}
